#pragma once

#include <stdlib.h>
#include <myo/myo.hpp>
#include "../JuceLibraryCode/JuceHeader.h"
#include "MyoSlice.h"
#include "../easylogging++.h"
#include "Parameter.h"
#include "Helpers.h"
#include <iostream>
#include <math.h>


namespace MyoToMidi {

	/*
		Abstract class with basic characteristics of a gesture.
	*/

	class Gesture {
	public:
		std::string toString();
		myo::Myo::VibrationType getVibrationType();
		myo::Pose getTrigger();
		virtual void setVibrationType();
		virtual juce::MidiMessage* generateMidi(MyoSlice* currentData) = 0; //returns a specific midi message based on this gesture's parameter, mode, and the myo data
	protected:
		Gesture(MyoSlice* initData, MyoSlice* offset, Parameter* param, int scalor);
		myo::Myo::VibrationType vibrationType;
		myo::Pose trigger;
		std::string name;
		MyoSlice* offsetData;
		int lastRawValue;
		Parameter* parameter;
		int scalor;
	};

	/*
		A complex gesture triggered and held by a fist, mapping roll data.
	*/
	class GrabKnob : public Gesture {
	public:
		GrabKnob(MyoSlice* initData, MyoSlice* offset, Parameter* param);
		GrabKnob(MyoSlice* initData, MyoSlice* offset, Parameter* param, int scalor);
		MidiMessage* generateMidi(MyoSlice* currentData) override;
	};

	/*
		A complex gesture triggered and held by spread fingers, mapping pitch.
	*/
	class OpenFader : public Gesture {
	public:
		OpenFader(MyoSlice* initData, MyoSlice* offset, Parameter* param);
		MidiMessage* generateMidi(MyoSlice* currentData) override;
	};

	/*
	A complex gesture triggered and held by spread fingers, mapping pitch.
	*/
	class GrabFader : public Gesture {
	public:
		GrabFader(MyoSlice* initData, MyoSlice* offset, Parameter* param);
		MidiMessage* generateMidi(MyoSlice* currentData) override;
	};

	/*
	A complex gesture triggered and held by a fist, mapping yaw
	*/
	class GrabSlider : public Gesture {
	public:
		GrabSlider(MyoSlice* initData, MyoSlice* offset, Parameter* param);
		MidiMessage* generateMidi(MyoSlice* currentData) override;
	};

	/*
	A complex gesture triggered and held by a fist, mapping yaw
	*/
	class OpenSlider : public Gesture {
	public:
		OpenSlider(MyoSlice* initData, MyoSlice* offset, Parameter* param);
		MidiMessage* generateMidi(MyoSlice* currentData) override;
	};

	/*
	A complex gesture triggered and held by a spread fingers, mapping roll
	*/
	class OpenKnob : public Gesture {
	public:
		OpenKnob(MyoSlice* initData, MyoSlice* offset, Parameter* param);
		OpenKnob(MyoSlice* initData, MyoSlice* offset, Parameter* param, int scalor);
		MidiMessage* generateMidi(MyoSlice* currentData) override;
	};

	/*
		A representation of the resting gesture.
	*/
	class Rest : public Gesture {
	public:
		Rest(MyoSlice* initData, MyoSlice* offset);
		MidiMessage* generateMidi(MyoSlice* currentData) override;
	};

	/*
	Listens for and maps peaks
	*/
	class PeakDetector : public Gesture {
	public:
		PeakDetector(MyoSlice* initData, MyoSlice* offset, Parameter * param);
		MidiMessage* generateMidi(MyoSlice* currentData) override;
	};

	class Center : public Gesture {
	public:
		Center(MyoSlice* initData, MyoSlice* offset);
		MidiMessage* generateMidi(MyoSlice* currentData) override;
		void setVibrationType() override;
	};
	class SelectiveSlider : public Gesture {
	public:
		SelectiveSlider(MyoSlice* initData, MyoSlice* offset, Parameter * param, myo::Pose trig);
		MidiMessage* generateMidi(MyoSlice* currentData) override;
	};
	class SelectiveFader : public Gesture {
	public:
		SelectiveFader(MyoSlice* initData, MyoSlice* offset, Parameter * param, myo::Pose trig);
		MidiMessage* generateMidi(MyoSlice* currentData) override;
	};
	class SelectiveXY : public Gesture {
	public:
		SelectiveXY(MyoSlice* initData, MyoSlice* offset, Parameter * param, myo::Pose trig);
		~SelectiveXY();
		MidiMessage* generateMidi(MyoSlice* currentData) override;
	};
}