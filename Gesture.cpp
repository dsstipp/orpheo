#include "Gesture.h"

namespace MyoToMidi {

	//======== Gesture =========//


	Gesture::Gesture(MyoSlice * initData, MyoSlice * offset, Parameter * param, int scalor)
	{
		setVibrationType();
		offsetData = offset;
		parameter = param;
		this->scalor = scalor;
	}

	std::string Gesture::toString()
	{
		std::ostringstream os;
		os << name;
		std::string s = os.str();
		return s;
	}

	myo::Myo::VibrationType Gesture::getVibrationType() {
		return vibrationType;
	}

	myo::Pose Gesture::getTrigger()
	{
		return trigger;
	}

	void Gesture::setVibrationType()
	{
		vibrationType = myo::Myo::VibrationType::vibrationShort;
	}

	//========= end Gesture =========//

	//========= GrabKnob ==============//

	GrabKnob::GrabKnob(MyoSlice * initData, MyoSlice * offset, Parameter * param) : GrabKnob::GrabKnob(initData, offset, param, 1)
	{
		
	}

	GrabKnob::GrabKnob(MyoSlice* initData, MyoSlice* offset, Parameter* param, int scalor) : Gesture::Gesture(initData, offset, param, scalor) {
		name = "GrabKnob";
		trigger = myo::Pose::fist;
		lastRawValue = initData->roll;
	}

	MidiMessage* GrabKnob::generateMidi(MyoSlice* currentData) {

		double deltaDouble = currentData->roll - lastRawValue;
		int delta = floor(deltaDouble);
		if (delta > 64)
			delta = delta - 128;

		int val = parameter->getValue() + (delta*scalor);
		if (val < 0)
			val = 0;
		else if (val > 127)
			val = 127;

		parameter->setValue(val);
		lastRawValue = currentData->roll;
		int number = parameter->getNumber();

		return &MidiMessage::controllerEvent(16, number, val);
	}

	//========= end GrabKnob ==========//

	//========= OpenFader ==============//

	OpenFader::OpenFader(MyoSlice* initData, MyoSlice* offset, Parameter* param) : Gesture::Gesture(initData, offset, param, 1) {
		name = "OpenFader";
		trigger = myo::Pose::fingersSpread;
		lastRawValue = initData->pitch;
	}

	MidiMessage* OpenFader::generateMidi(MyoSlice* currentData) {

		double deltaDouble = currentData->pitch - lastRawValue;
		int delta = floor(deltaDouble);
		if (delta > 64)
			delta = delta - 128;

		int val = parameter->getValue() + delta;
		if (val < 0)
			val = 0;
		else if (val > 127)
			val = 127;

		parameter->setValue(val);
		lastRawValue = currentData->pitch;
		int number = parameter->getNumber();

		return &MidiMessage::controllerEvent(16, number, val);
	}

	//========= end OpenFader ==========//

	//========= GrabFader ==============//

	GrabFader::GrabFader(MyoSlice* initData, MyoSlice* offset, Parameter* param) : Gesture::Gesture(initData, offset, param, 1) {
		name = "GrabFader";
		trigger = myo::Pose::fist;
		lastRawValue = initData->pitch;
	}

	MidiMessage* GrabFader::generateMidi(MyoSlice* currentData) {

		double deltaDouble = currentData->pitch - lastRawValue;
		int delta = floor(deltaDouble);
		if (delta > 64)
			delta = delta - 128;

		int val = parameter->getValue() + delta;
		if (val < 0)
			val = 0;
		else if (val > 127)
			val = 127;

		parameter->setValue(val);
		lastRawValue = currentData->pitch;
		int number = parameter->getNumber();

		return &MidiMessage::controllerEvent(16, number, val);
	}

	//========= end GrabFader ==========//

	//========== OpenSlider ========//
	OpenSlider::OpenSlider(MyoSlice * initData, MyoSlice * offset, Parameter * param) : Gesture::Gesture(initData, offset, param, 1)
	{
		name = "OpenSlider";
		trigger = myo::Pose::fingersSpread;
		lastRawValue = initData->yaw + (64 - offset->yaw);
		if (lastRawValue > 128)
			lastRawValue = lastRawValue - 128;
		else if (lastRawValue < 0)
			lastRawValue = 128 + lastRawValue;
	}

	MidiMessage * OpenSlider::generateMidi(MyoSlice * currentData)
	{

		double shiftedYaw = currentData->yaw + (64 - offsetData->yaw);
		if (shiftedYaw > 128)
			shiftedYaw = shiftedYaw - 128;
		else if (shiftedYaw < 0)
			shiftedYaw = 128 + shiftedYaw;

		double delta = shiftedYaw - lastRawValue;
		if (delta > 64)
			delta = delta - 128;

		int val = parameter->getValue() + (delta*2);
		if (val < 0)
			val = 0;
		else if (val > 127)
			val = 127;

		lastRawValue = shiftedYaw;
		parameter->setValue(val);
		int number = parameter->getNumber();

		return &MidiMessage::controllerEvent(16, number, val);
	}

	//========== end OpenSlider =======//

	//========== GrabSlider ========//
	GrabSlider::GrabSlider(MyoSlice * initData, MyoSlice * offset, Parameter * param) : Gesture::Gesture(initData, offset, param, 1)
	{
		name = "GrabSlider";
		trigger = myo::Pose::fist;
		lastRawValue = initData->yaw + (64 - offset->yaw);
		if (lastRawValue > 128)
			lastRawValue = lastRawValue - 128;
		else if (lastRawValue < 0)
			lastRawValue = 128 + lastRawValue;
	}

	MidiMessage * GrabSlider::generateMidi(MyoSlice * currentData)
	{

		double shiftedYaw = currentData->yaw + (64 - offsetData->yaw);
		if (shiftedYaw > 128)
			shiftedYaw = shiftedYaw - 128;
		else if (shiftedYaw < 0)
			shiftedYaw = 128 + shiftedYaw;

		double delta = shiftedYaw - lastRawValue;
		if (delta > 64)
			delta = delta - 128;

		int val = parameter->getValue() + (delta*2);
		if (val < 0)
			val = 0;
		else if (val > 127)
			val = 127;

		lastRawValue = shiftedYaw;
		parameter->setValue(val);
		int number = parameter->getNumber();

		return &MidiMessage::controllerEvent(16, number, val);
	}

	//========== end GrabSlider =======//

	//========== OpenKnob =========//
	OpenKnob::OpenKnob(MyoSlice * initData, MyoSlice * offset, Parameter * param) : OpenKnob::OpenKnob(initData, offset, param, 1)
	{
		
	}

	OpenKnob::OpenKnob(MyoSlice * initData, MyoSlice * offset, Parameter * param, int scalor) : Gesture::Gesture(initData, offset, param, scalor)
	{
		name = "OpenKnob";
		trigger = myo::Pose::fingersSpread;
		lastRawValue = initData->roll;
	}

	MidiMessage * OpenKnob::generateMidi(MyoSlice * currentData)
	{
		double deltaDouble = currentData->roll - lastRawValue;
		int delta = floor(deltaDouble);
		if (delta > 64)
			delta = delta - 128;

		int val = parameter->getValue() + (delta*scalor);
		if (val < 0)
			val = 0;
		else if (val > 127)
			val = 127;

		parameter->setValue(val);
		lastRawValue = currentData->roll;
		int number = parameter->getNumber();

		return &MidiMessage::controllerEvent(16, number, val);

	}
	//======= end OpenKnob ========//

	//========= Rest ==============//

	Rest::Rest(MyoSlice* initData, MyoSlice* offset) : Gesture::Gesture(initData, offset, nullptr, 1) {
		name = "Rest";
		trigger = myo::Pose::rest;
	}

	MidiMessage * Rest::generateMidi(MyoSlice * currentData)
	{
		return nullptr;
	}

	//========= end Rest ==========//

	//========= PeakDetector ==============//

	PeakDetector::PeakDetector(MyoSlice* initData, MyoSlice* offset, Parameter * param) : Gesture::Gesture(initData, offset, nullptr, 1) {
		name = "PeakDetector";
		trigger = myo::Pose::unknown;
		parameter = param;
		initData->lastPeak = Clock::now();
	}

	MidiMessage * PeakDetector::generateMidi(MyoSlice * currentData)
	{
		int number = parameter->getNumber();
		MidiMessage* m;
		if (currentData->isPeak())
			m = &juce::MidiMessage::noteOn(16, number, 100.0f);
		else
			m = &juce::MidiMessage();
		return m;
	}

	//========= end PeakDetector ==========//

	//============ Center ===============//

	Center::Center(MyoSlice* initData, MyoSlice * offset) : Gesture::Gesture(initData, offset, nullptr, 1)
	{
		name = "Center";
		trigger = myo::Pose::doubleTap;
		offset->yaw = initData->yaw;
		offset->roll = 64;
		offset->pitch = 64;
	}

	MidiMessage * Center::generateMidi(MyoSlice * currentData)
	{
		return nullptr;
	}

	void Center::setVibrationType() {
		vibrationType = myo::Myo::VibrationType::vibrationLong;
	}

	//========== end Center ================//

	SelectiveSlider::SelectiveSlider(MyoSlice* initData, MyoSlice* offset, Parameter * param, myo::Pose trig) : Gesture::Gesture(initData, offset, nullptr, 1) {
		name = "SelectiveSlider";
		trigger = trig;
		parameter = param;
	}

	MidiMessage * SelectiveSlider::generateMidi(MyoSlice * currentData)
	{

		double shiftedYaw = currentData->yaw + (64 - offsetData->yaw);
		if (shiftedYaw > 128)
			shiftedYaw = shiftedYaw - 128;
		else if (shiftedYaw < 0)
			shiftedYaw = 128 + shiftedYaw;

		int value = parameter->getValue();
		if (shiftedYaw < 64) {
			if (value == 38)
				return nullptr;
			value = 38;
		}
		else {
			if (value == 39)
				return nullptr;
			value = 39;
		}
		parameter->setValue(value);

		MidiMessage * m = &juce::MidiMessage::noteOn(16, value, 100.0f);
		return m;
	}

	SelectiveFader::SelectiveFader(MyoSlice* initData, MyoSlice* offset, Parameter * param, myo::Pose trig) : Gesture::Gesture(initData, offset, nullptr, 1) {
		name = "SelectiveFader";
		trigger = trig;
		parameter = param;
	}

	MidiMessage * SelectiveFader::generateMidi(MyoSlice * currentData)
	{

		int value = parameter->getValue();
		if (currentData->pitch < 64) {
			if (value == 36)
				return nullptr;
			value = 36;
		}
		else {
			if (value == 37)
				return nullptr;
			value = 37;
		}
		parameter->setValue(value);

		MidiMessage * m = &juce::MidiMessage::noteOn(16, value, 100.0f);
		return m;
	}

	SelectiveXY::SelectiveXY(MyoSlice* initData, MyoSlice* offset, Parameter * param, myo::Pose trig) : Gesture::Gesture(initData, offset, nullptr, 1) {
		name = "SelectiveXY";
		trigger = trig;
		parameter = param;
		param->setValue(param->getDefaultValue());
	}

	SelectiveXY::~SelectiveXY()
	{
		parameter->setValue(0);
	}

	MidiMessage * SelectiveXY::generateMidi(MyoSlice * currentData)
	{
		double shiftedYaw = currentData->yaw + (64 - offsetData->yaw);
		if (shiftedYaw > 128)
			shiftedYaw = shiftedYaw - 128;
		else if (shiftedYaw < 0)
			shiftedYaw = 128 + shiftedYaw;

		int value = parameter->getValue();

		
		if (currentData->pitch < 64) {
			if (shiftedYaw < 64) { //q 3
				if (value == 36)
					return nullptr;
				value = 36;
			}
			else {//q 4
				if (value == 37)
					return nullptr;
				value = 37;
			}
		}
		else {
			if (shiftedYaw > 64) { //q 1 
				if (value == 38)
					return nullptr;
				value = 38;
			}
			else { //q2 
				if (value == 39)
					return nullptr;
				value = 39;
			}
		}
		parameter->setValue(value);

		MidiMessage * m = &juce::MidiMessage::noteOn(16, value, 100.0f);
		return m;
	}
}