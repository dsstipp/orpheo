#pragma once

#include <stdlib.h>
#include <iostream>
#include <map>

#include "../JuceLibraryCode/JuceHeader.h"
#include "Gesture.h"
#include <myo/myo.hpp>
#include "Parameter.h"


//GLOBAL VARIABLES
namespace MyoToMidi {
	static juce::MidiOutput *MIDIOUT;
	static juce::MidiInput *MIDIIN;
	static int MIDICHANNEL;
	static std::map<std::string, Parameter> PARAMETERS = {
		{ "1open", Parameter("1Filter",  14, 0) },
		{ "1grab", Parameter("1Volume", 15, 0) },
		{ "2open", Parameter("2Spectrum", 24, 0) },
		{ "2grab", Parameter("AFilterLFO", 23, 0) },
		{ "3open", Parameter("3Wah", 25, 0) },
		{ "3grab", Parameter("3Distortion", 26, 0) },
		{ "4open", Parameter("4Delay", 20, 0 ) },
		{ "4grab", Parameter("4Overdrive", 21, 0) },
		{ "5grab", Parameter("5Triggers", 0, 0) },
		//{ "5open", Parameter("GlobalTempo", 22, 126) },

	};

}