#include "Manager.h"

namespace MyoToMidi {

	Manager::Manager(MidiOutput* output, myo::Myo* theMyo, MyoSlice* myoData) {

		out = output;
		myo = theMyo;
		data = myoData;

		MidiBuffer b = MidiBuffer();

		//initialize parameters to default values
		Parameter* currentParam;
		int sample = 0;
		for (std::map<std::string, Parameter>::iterator it = PARAMETERS.begin(); it != PARAMETERS.end(); ++it) {
			currentParam = &it->second;
			int number = currentParam->getNumber();
			juce::MidiMessage* m = &juce::MidiMessage::controllerEvent(16, number, currentParam->getDefaultValue());
			if (number != 0) {
				b.addEvent(*m, sample++);
			}
		}
		currentMode = new Mode1();
		b.addEvent(*currentMode->generateMidi(), sample++);

		out->sendBlockOfMessagesNow(b);

		offsetData = new MyoSlice(data);
		currentState = new Center(data, offsetData);
		LOG(INFO) << "Centered";
		LOG(INFO) << "Mode: Mode1";

		isActive = true;

	}

	Manager::~Manager()
	{
		delete offsetData;
	}

	/*
	Function that is called every time the myo hub pauses
	*/
	void Manager::update() {
		parseMidiIn();
		if (isActive) {
			transition();
			send();
		}
	}

	void Manager::transition() {
		//check if mode changed
		Mode* newMode = currentMode->transitionMode(data);

		if (newMode != currentMode) {

			//unarm previous instrument if relevant
			juce::MidiMessage * unarmOn = currentMode->generateMidi();
			if (unarmOn) {
				out->sendMessageNow(*unarmOn);
				//LOG(INFO) << unarmOn->getDescription();
			}

			//update the mode
		 	delete currentMode;
			currentMode = newMode;

			//arm new instrument
			juce::MidiMessage* armOn = currentMode->generateMidi();
			if (armOn) {
				out->sendMessageNow(*armOn);
				//LOG(INFO) << armOn->getDescription();
			}

			//feedback
			myo->vibrate(myo::Myo::vibrationMedium);
			myo->vibrate(myo::Myo::vibrationShort);
			LOG(INFO) << "Mode: " << currentMode->toString();

			//wait, because mode change gesture is not a one shot
			Sleep(1000);

			//note off to match each note on
			if (armOn) {
				juce::MidiMessage armOff = juce::MidiMessage::noteOff(16, armOn->getNoteNumber());
				out->sendMessageNow(armOff);
			}

			if (unarmOn) {
				juce::MidiMessage unarmOff = juce::MidiMessage::noteOff(16, unarmOn->getNoteNumber());
				out->sendMessageNow(unarmOff);
			}

			//we dynamically allocated them, so
			delete armOn;
			delete unarmOn;
		}

		Gesture* newState = currentMode->transitionGesture(currentState, data, offsetData);

		//if we changed states,
		if (newState != currentState) {
			delete currentState;
			currentState = newState;

			myo->vibrate(currentState->getVibrationType());

			LOG(INFO) << "Gesture: " << currentState->toString();
		}

	}

	void Manager::send() {

		//use the current gesture's def of the correct midi message
		juce::MidiMessage* message = currentState->generateMidi(data);
		//if we have a message to send,
		if (message) {
			juce::MidiMessage m = *message;
			out->sendMessageNow(m);
			if (m.isNoteOn()) {
				Sleep(50);
				out->sendMessageNow(MidiMessage::noteOff(16, m.getNoteNumber()));
			}
		}

	}

	void Manager::parseMidiIn()
	{
		juce::MidiMessage* m = data->lastInput;
		//user 1, (0, 7), enable/disable mapping
		if (m && m->getChannel() == 1 && m->getNoteNumber() == 7 && m->isNoteOn()) {
			isActive = !isActive;
			myo->vibrate(myo::Myo::vibrationShort);

			if (isActive) {
				myo->vibrate(myo::Myo::vibrationShort);
				LOG(INFO) << "MAPPING ON";
			}
			else {
				LOG(INFO) << "MAPPING OFF";
			}
			data->lastInput = nullptr;
		}
	}

}