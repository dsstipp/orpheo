#include "Parameter.h"

namespace MyoToMidi {
	Parameter::Parameter()
	{
	}

	Parameter::Parameter(std::string name, int num, int defaultVal)
	{
		this->name = name;
		defaultValue = defaultVal;
		currentValue = defaultValue;
		number = num;
	}

	std::string Parameter::getName()
	{
		return name;
	}

	int Parameter::getValue()
	{
		return currentValue;
	}

	void Parameter::setValue(int newVal)
	{
		currentValue = newVal;
	}

	int Parameter::getNumber() {
		return number;
	}

	int Parameter::getDefaultValue()
	{
		return defaultValue;
	}
}