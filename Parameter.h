#pragma once

#include <stdlib.h>
#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"

/*
	A class to represent controllable values in our DAW.
*/
namespace MyoToMidi {
	class Parameter {
	public:
		Parameter(); // should not be used
		Parameter(std::string name, int num, int defaultVal);
		std::string getName();
		int getValue(); //get the current value of this param
		void setValue(int newVal); //set the current value of this param
		int getDefaultValue();
		int getNumber();
	private:
		std::string name; //name of the parameter
		int defaultValue; //default value to be used for initializing
		int currentValue; // tracks the parameter as we change it
		int number; //this parameter's cc number
	};
}