#pragma once

#define _USE_MATH_DEFINES

#include <stdlib.h>
#include <myo/myo.hpp>
#include "../easylogging++.h"
#include "MyoSlice.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include "Helpers.h"


namespace MyoToMidi {

	class DataCollector : public myo::DeviceListener {
	public:
		DataCollector();

		DataCollector(MyoSlice* myoData);

		void onUnpair(myo::Myo* myo, uint64_t timestamp);

		void onOrientationData(myo::Myo* myo, uint64_t timestamp, const myo::Quaternion<float>& quat);

		void onAccelerometerData(myo::Myo * myo, uint64_t timestamp, const myo::Vector3<float>& accel);

		void onPose(myo::Myo* myo, uint64_t timestamp, myo::Pose pose);

		void onArmSync(myo::Myo* myo, uint64_t timestamp, myo::Arm arm, myo::XDirection xDirection, float rotation,
			myo::WarmupState warmupState);

		void onArmUnsync(myo::Myo* myo, uint64_t timestamp);

		void onUnlock(myo::Myo* myo, uint64_t timestamp);

		void onLock(myo::Myo* myo, uint64_t timestamp);

		void update();

		void logInfo();


		// These values are set by onArmSync() and onArmUnsync() above.
		bool onArm;

		// This is set by onUnlocked() and onLocked() above.
		bool isUnlocked;

		// These values are set by onOrientationData() and onPose() above.
		double roll_w, pitch_w, yaw_w;
		myo::Pose currentPose;
		MyoSlice* data;

	};
	
	class MidiListener : public juce::MidiInputCallback {
	public:
		MidiListener(MyoSlice* myoData);
		void handleIncomingMidiMessage(MidiInput *source, const MidiMessage &message);
	private:
		MyoSlice* data;
	};
}