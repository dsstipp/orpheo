#include "DataCollector.h"

namespace MyoToMidi {

		DataCollector::DataCollector()
			: onArm(false), isUnlocked(false), roll_w(0), pitch_w(0), yaw_w(0), currentPose()
		{
		}

		DataCollector::DataCollector(MyoSlice* myoData)
			: onArm(false), isUnlocked(false), roll_w(0), pitch_w(0), yaw_w(0), currentPose()
		{
			data = myoData;
		}

		void DataCollector::onUnpair(myo::Myo* myo, uint64_t timestamp)
		{

			LOG(INFO) << "UNPAIRED";

			roll_w = 0;
			pitch_w = 0;
			yaw_w = 0;
			onArm = false;
			isUnlocked = false;
		}

		void DataCollector::onOrientationData(myo::Myo* myo, uint64_t timestamp, const myo::Quaternion<float>& quat)
		{
			using std::atan2;
			using std::asin;
			using std::sqrt;
			using std::max;
			using std::min;

			// Calculate Euler angles (roll, pitch, and yaw) from the unit quaternion.
			float roll = atan2(2.0f * (quat.w() * quat.x() + quat.y() * quat.z()),
				1.0f - 2.0f * (quat.x() * quat.x() + quat.y() * quat.y()));
			float pitch = asin(max(-1.0f, min(1.0f, 2.0f * (quat.w() * quat.y() - quat.z() * quat.x()))));
			float yaw = atan2(2.0f * (quat.w() * quat.z() + quat.x() * quat.y()),
				1.0f - 2.0f * (quat.y() * quat.y() + quat.z() * quat.z()));

			//convert floating point angles into a useful scaled number.
			roll_w = static_cast<double>((128 - (roll + (float)M_PI) / (M_PI * 2.0f) * 128));
			pitch_w = static_cast<double>((pitch + (float)M_PI / 2.0f) / M_PI * 128);
			yaw_w = static_cast<double>((128 - (yaw + (float)M_PI) / (M_PI * 2.0f) * 128));

		}

		void DataCollector::onAccelerometerData(myo::Myo * myo, uint64_t timestamp, const myo::Vector3<float>& accel)
		{
			data->updateAccelCache(accel.magnitude());
		}


		void DataCollector::logInfo() {
			LOG(INFO) << "(" << roll_w << ", " << pitch_w << "," << yaw_w, ")";
		}

		void DataCollector::onPose(myo::Myo* myo, uint64_t timestamp, myo::Pose pose)
		{
			currentPose = pose;

		}

		void DataCollector::onArmSync(myo::Myo* myo, uint64_t timestamp, myo::Arm arm, myo::XDirection xDirection, float rotation,
			myo::WarmupState warmupState)
		{

			LOG(INFO) << "ARM SYNCED";

			onArm = true;
		}

		void DataCollector::onArmUnsync(myo::Myo* myo, uint64_t timestamp)
		{

			LOG(INFO) << "ARM UNSYNCED";

			onArm = false;
		}

		void DataCollector::onUnlock(myo::Myo* myo, uint64_t timestamp)
		{

			LOG(INFO) << "UNLOCKED";

			isUnlocked = true;
		}

		void DataCollector::onLock(myo::Myo* myo, uint64_t timestamp)
		{

			LOG(INFO) << "LOCKED";

			isUnlocked = false;
		}

		void DataCollector::update() {
			data->pitch = pitch_w;
			data->pose = currentPose;
			data->roll = roll_w;
			data->yaw = yaw_w;
		}


		MidiListener::MidiListener(MyoSlice * myoData)
		{
			data = myoData;
		}

		void MidiListener::handleIncomingMidiMessage(MidiInput * source, const MidiMessage & message)
		{
			data->lastInput = new MidiMessage(message);
			
		}

}