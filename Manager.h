#pragma once

#include <stdlib.h>
#include <../JuceLibraryCode/JuceHeader.h>
#include <myo/myo.hpp>

#include "Gesture.h"
#include "MyoSlice.h"
#include "../easylogging++.h"
#include "Mode.h"
#include "Helpers.h"

/*
	Responsible for managing modes, IO, and gestures.	
*/

namespace MyoToMidi {
	
	class Manager {
	public:

		Manager(MidiOutput* output, myo::Myo* theMyo, MyoSlice* myoData);
		~Manager();
		void update(); //called on every window of time
		void transition(); //handles all transitioning logic
		void send(); //handles output logic
		void parseMidiIn(); //handles input logic

	private:
		Gesture *currentState;
		MidiOutput *out;
		myo::Myo *myo;
		MyoSlice *data;
		MyoSlice *offsetData;
		Mode* currentMode;
		bool isActive; //when true, we listen and respond to myo data
	};

}