#pragma once
#pragma once

#include <stdlib.h>
#include <iostream>
#include "Gesture.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include <chrono>

/*
	A class to represent modes, or mappings from gesture to parameter.
*/

namespace MyoToMidi {
	class Mode {
	public:
		virtual std::string toString() = 0; 
		virtual Gesture* transitionGesture(Gesture* currentState, MyoSlice* currentData, MyoSlice* offsetData) = 0;
		virtual Mode* transitionMode(MyoSlice* data) = 0;
		MidiMessage* generateMidi();
	protected:
		MidiMessage initialMidi;
	};

	class Mode1 : public Mode {
	public:
		Mode1();
		std::string toString();
		Gesture *transitionGesture(Gesture* currentState, MyoSlice* currentData, MyoSlice* offsetData) override;
		Mode* transitionMode(MyoSlice* data) override;
	};

	class Mode2 : public Mode {
	public:
		Mode2();
		std::string toString();
		Gesture *transitionGesture(Gesture* currentState, MyoSlice* currentData, MyoSlice* offsetData) override;
		Mode* transitionMode(MyoSlice* data) override;
	};
	
	class Mode3 : public Mode {
	public:
		Mode3();
		std::string toString();
		Gesture *transitionGesture(Gesture* currentState, MyoSlice* currentData, MyoSlice* offsetData) override;
		Mode* transitionMode(MyoSlice* data) override;
	};

	class Mode4 : public Mode {
	public:
		Mode4();
		std::string toString();
		Gesture *transitionGesture(Gesture* currentState, MyoSlice* currentData, MyoSlice* offsetData) override;
		Mode* transitionMode(MyoSlice* data) override;
	};

	class Mode5 : public Mode {
	public:
		Mode5();
		std::string toString();
		Gesture *transitionGesture(Gesture* currentState, MyoSlice* currentData, MyoSlice* offsetData) override;
		Mode* transitionMode(MyoSlice* data) override;
	};
}