#define _USE_MATH_DEFINES

#include <iostream>
#include <array>
#include <sstream>
#include <stdexcept>

#include "../JuceLibraryCode/JuceHeader.h"
#include "../easylogging++.h"
#include <myo/myo.hpp>
#include "../tinyxml2.h"

#include "DataCollector.h"
#include "Manager.h"
#include "MyoSlice.h"
#include "Helpers.h"
#include "Parameter.h"

INITIALIZE_EASYLOGGINGPP

//==============================================================================

// Configure Easyloggingpp
void configureLogger() {

	el::Configurations conf;
	conf.setToDefault();

	conf.setGlobally(el::ConfigurationType::Filename, "logs/my.log");

	conf.setGlobally(el::ConfigurationType::Format, "%datetime %msg");
	conf.setGlobally(el::ConfigurationType::MaxLogFileSize, "2097152"); //2MB
	conf.setGlobally(el::ConfigurationType::ToFile, "FALSE");

	el::Loggers::reconfigureLogger("default", conf);

};

/*
function to open the midi port with the passed name
*/
void openMidiPorts(std::string inName, std::string outName, MyoToMidi::MidiListener* c) {

	juce::StringArray ins = MidiInput::getDevices();

	for (int i = 0; i < ins.size(); i++) {
		if (ins[i].contains(StringRef(inName))) {
			MyoToMidi::MIDIIN = MidiInput::openDevice(i, c);
		}
	}

	juce::StringArray outs = MidiOutput::getDevices();

	for (int i = 0; i < outs.size(); i++) {
		if (outs[i].contains(StringRef(outName))) {
			MyoToMidi::MIDIOUT = MidiOutput::openDevice(i);
		}
	}

}

int main(int argc, char* argv[])
{

	configureLogger();

	try {

		//load and parse config file
		tinyxml2::XMLDocument doc;
		doc.LoadFile("../../config.xml");
		tinyxml2::XMLNode * root = doc.FirstChild();
		std::string inName = root->FirstChildElement("MIDIIN")->GetText();
		std::string outName = root->FirstChildElement("MIDIOUT")->GetText();

		//establish myo connection
		myo::Hub hub("com.dsstipp.MyoToMidi");
		myo::Myo* myo = hub.waitForMyo(10000);
		if (!myo) {
			throw std::runtime_error("Unable to find a Myo!");
		}
		LOG(INFO) << "Connected to Myo!" << std::endl;

		MyoToMidi::MyoSlice data; //this will store the current time frame of myo information
		
		MyoToMidi::DataCollector collector(&data); //tell the collector to listen
		hub.addListener(&collector);

		//callback for the midi input
		MyoToMidi::MidiListener* callback = new MyoToMidi::MidiListener(&data);
		openMidiPorts(inName, outName, callback);

		if (!MyoToMidi::MIDIOUT)
			throw std::runtime_error("Unable to open requested midi out port");
		else
			LOG(INFO) << "Opened Midi Out";

		if (!MyoToMidi::MIDIIN) {
			LOG(INFO) << "Unable to open midi in";
		}
		else {
			MyoToMidi::MIDIIN->start();
			LOG(INFO) << "Opened Midi In";
		}

		hub.setLockingPolicy(myo::Hub::lockingPolicyNone);
		hub.run(50);
		collector.update();
		MyoToMidi::Manager manager(MyoToMidi::MIDIOUT, myo, &data); //this manages our states and stuff
		
		while (1) {

			//listen to the myo
			hub.run(50);

			//update var data with the current myo info
			collector.update();

			//tell the manager to do stuff with the info
			manager.update();

		}
	}
	catch (const std::exception& e) { 		// If a standard exception occurred, we print out its message and exit.
		std::cerr << "Error: " << e.what() << std::endl;
		std::cerr << "Press enter to continue.";
		std::cin.ignore();
		return 1;
	}

	return 0;
}