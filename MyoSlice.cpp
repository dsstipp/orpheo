#include "MyoSlice.h"

namespace MyoToMidi {
	MyoSlice::MyoSlice()
	{
		lastInput = new juce::MidiMessage();
	}

	MyoSlice::MyoSlice(MyoSlice* toCopy)
	{
		roll = toCopy->roll;
		pitch = toCopy->pitch;
		yaw = toCopy->yaw;
		pose = toCopy->pose;
		lastInput = new juce::MidiMessage();
		lastPeak = Clock::now();
	}

	std::string MyoSlice::toString() {
		std::ostringstream os;
		os << roll << ", " << pitch << ", " << yaw << ", " << pose;
		std::string s = os.str();
		return s;
	}

	void MyoSlice::updateAccelCache(float num)
	{
		for (int i = 0; i < 4; i++) {
			accel_cache[i + 1] = accel_cache[i];
		}
		accel_cache[0] = num;
	}

	std::string MyoSlice::printCache()
	{
		std::ostringstream os;
		for (int i = 0; i < 5; i++) {
			os << accel_cache[i];
			os << ", ";
		}
		std::string s = os.str();
		return s;
	}

	bool MyoSlice::isPeak()
	{
		float avg = (accel_cache[0] + accel_cache[1] + accel_cache[3] + accel_cache[4]) / 4;

		Clock::time_point current = Clock::now();

		auto elapsed = (std::chrono::duration_cast<std::chrono::nanoseconds>(current - lastPeak).count()) / 1000000;

		if (((accel_cache[2] - avg) > .05f) && (elapsed > 190)) {
			LOG(INFO) << "PEAK" << elapsed;
			lastPeak = Clock::now();
			return true;
		}
		else {
			return false;
		}
	}

	void MyoSlice::flushAccelCache()
	{
		for (int i = 0; i < 5; i++) {
			accel_cache[i] = 0;
		}
	}
	
}