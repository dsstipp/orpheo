#include "Mode.h"

namespace MyoToMidi {


	juce::MidiMessage * Mode::generateMidi()
	{
		if (initialMidi.isNoteOn())
			return new juce::MidiMessage(initialMidi);
		else return nullptr;
	}

	//======= Mode1 ==========//	

	Mode1::Mode1() {
		initialMidi = juce::MidiMessage::noteOn(16, 1, 100.0f);	
	}
	
	std::string Mode1::toString()
	{
		return "Mode1";
	}

	Gesture * Mode1::transitionGesture(Gesture * currentState, MyoSlice * currentData, MyoSlice * offsetData)
	{
		if (currentData->pose == currentState->getTrigger()) { //no transition
			return currentState;
		}
		else if (currentState->getTrigger() == myo::Pose::rest) { //currently resting
			if (currentData->pose == myo::Pose::fingersSpread) { 
				return new OpenFader(currentData, offsetData, &PARAMETERS["1open"]);
			}
			else if (currentData->pose == myo::Pose::fist) {
				return new GrabKnob(currentData, offsetData, &PARAMETERS["1grab"], 2);
			}
			else if (currentData->pose == myo::Pose::doubleTap) {
				return new Center(currentData, offsetData);
			}
			else {
				return currentState;
			}
		}
		else { //go to rest. mode change or gesture exit.
			return new Rest(nullptr, offsetData);
		}
	}

	Mode * Mode1::transitionMode(MyoSlice* data)
	{
		if (data->pose == myo::Pose::waveIn) {
			return new Mode4();
		}
		else if (data->pose == myo::Pose::waveOut) {
			return new Mode2();
		}
		else {
			return this;
		}
	}


	//======== Mode2 =======//

	Mode2::Mode2() {
		initialMidi = juce::MidiMessage::noteOn(16, 2, 100.0f);
	}

	std::string Mode2::toString()
	{
		return "Mode2";
	}

	Gesture * Mode2::transitionGesture(Gesture * currentState, MyoSlice * currentData, MyoSlice * offsetData)
	{
		if (currentData->pose == currentState->getTrigger()) {	//no transition
			return currentState;
		}
		else if (currentState->getTrigger() == myo::Pose::rest) { //resting
			if (currentData->pose == myo::Pose::fist) {
				return new GrabKnob(currentData, offsetData, &PARAMETERS["2grab"], 2);
			}
			else if (currentData->pose == myo::Pose::fingersSpread) {
				return new OpenSlider(currentData, offsetData, &PARAMETERS["2open"]);
			}
			else if (currentData->pose == myo::Pose::doubleTap) {
				return new Center(currentData, offsetData);
			}
			else {
				return currentState;
			}
		}
		else { //go to rest. mode change or gesture exit.
			return new Rest(nullptr, offsetData);
		}
	}

	Mode * Mode2::transitionMode(MyoSlice* data)
	{
		if (data->pose == myo::Pose::waveIn) {
			return new Mode1();
		}
		else if (data->pose == myo::Pose::waveOut) {
			return new Mode5();
		}
		else {
			return this;
		}
	}

	//========= Mode3 ========//
	Mode3::Mode3() {
		initialMidi = juce::MidiMessage::noteOn(16, 3, 100.0f);
	}

	std::string Mode3::toString()
	{
		return "Mode3";
	}

	Gesture * Mode3::transitionGesture(Gesture * currentState, MyoSlice * currentData, MyoSlice * offsetData)
	{
		//no transition
		if (currentData->pose == currentState->getTrigger()) {
			return currentState;
		}
		else if (currentState->getTrigger() == myo::Pose::rest) { //resting
			if (currentData->pose == myo::Pose::fingersSpread) {
				return new OpenKnob(currentData, offsetData, &PARAMETERS["3open"], 2);
			}
			else if (currentData->pose == myo::Pose::fist) {
				return new GrabFader(currentData, offsetData, &PARAMETERS["3grab"]);
			}
			else if (currentData->pose == myo::Pose::doubleTap) {
				return new Center(currentData, offsetData);
			}
			else {
				return currentState;
			}
		}
		else { //go to rest. mode change or gesture exit.
			return new Rest(nullptr, offsetData);
		}
	}

	Mode * Mode3::transitionMode(MyoSlice* data)
	{
		if (data->pose == myo::Pose::waveIn) {
			return new Mode5();
		}
		else if (data->pose == myo::Pose::waveOut) {
			return new Mode4();
		}
		else {
			return this;
		}
	}

	Mode4::Mode4()
	{
		initialMidi = juce::MidiMessage::noteOn(16, 4, 100.0f);

	}

	std::string Mode4::toString()
	{
		return "Mode4";
	}

	Gesture * Mode4::transitionGesture(Gesture * currentState, MyoSlice * currentData, MyoSlice * offsetData)
	{
		if (currentData->pose == currentState->getTrigger()) {	//no transition
			return currentState;
		}
		else if (currentState->getTrigger() == myo::Pose::rest) { //resting
			if (currentData->pose == myo::Pose::fingersSpread) {
				return new OpenSlider(currentData, offsetData, &PARAMETERS["4open"]);
			}
			 else if (currentData->pose == myo::Pose::fist) {
				 return new GrabKnob(currentData, offsetData, &PARAMETERS["4grab"], 2);
			 }
			else if (currentData->pose == myo::Pose::doubleTap) {
				return new Center(currentData, offsetData);
			}
			else {
				return currentState;
			}
		}
		else { //go to rest. mode change or gesture exit.
			return new Rest(nullptr, offsetData);
		}
	}

	Mode * Mode4::transitionMode(MyoSlice * data)
	{
		if (data->pose == myo::Pose::waveIn) {
			return new Mode3();
		}
		else if (data->pose == myo::Pose::waveOut) {
			return new Mode1();
		}
		else {
			return this;
		}
	}

	Mode5::Mode5()
	{
		initialMidi = juce::MidiMessage::noteOn(16, 5, 100.0f);

	}

	std::string Mode5::toString()
	{
		return "Mode5";
	}

	Gesture * Mode5::transitionGesture(Gesture * currentState, MyoSlice * currentData, MyoSlice * offsetData)
	{
		if (currentData->pose == currentState->getTrigger()) {	//no transition
			return currentState;
		}
		else if (currentState->getTrigger() == myo::Pose::rest) { //resting
			//if (currentData->pose == myo::Pose::fingersSpread) {
			//	return new OpenSlider(currentData, offsetData, &PARAMETERS["5open"]);
			//}
			if (currentData->pose == myo::Pose::fist) {
				return new SelectiveXY(currentData, offsetData, &PARAMETERS["5grab"], myo::Pose::fist);
			}
			else if (currentData->pose == myo::Pose::doubleTap) {
				return new Center(currentData, offsetData);
			}
			else {
				return currentState;
			}
		}
		else { //go to rest. mode change or gesture exit.
			return new Rest(nullptr, offsetData);
		}
	}

	Mode * Mode5::transitionMode(MyoSlice * data)
	{
		if (data->pose == myo::Pose::waveIn) {
			return new Mode2();
		}
		else if (data->pose == myo::Pose::waveOut) {
			return new Mode3();
		}
		else {
			return this;
		}
	}
}