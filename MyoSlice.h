#pragma once

#include <myo/myo.hpp>
#include "../easylogging++.h"
#include <chrono>
#include "../JuceLibraryCode/JuceHeader.h"

typedef std::chrono::high_resolution_clock Clock;

namespace MyoToMidi {

	/*
	A class to store the relevant myo information for a slice in time.
	*/
	class MyoSlice {
	public:
		MyoSlice();
		MyoSlice(MyoSlice* toCopy);
		std::string toString();
		void updateAccelCache(float num);
		std::string printCache();
		bool isPeak();
		void flushAccelCache(); // not currently used?

		double roll, pitch, yaw;
		myo::Pose pose;
		Clock::time_point lastPeak;
		float accel; // not currently used?
		float accel_cache[5] = { 0, 0, 0, 0, 0 };
		juce::MidiMessage* lastInput;

		
	};

}